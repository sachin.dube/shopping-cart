package com.ecommerce.shoppingcart

import scala.math.BigDecimal.RoundingMode

object ShoppingCart {

  private val productPriceList: Map[String, BigDecimal] = Map(("apple", 0.60), ("orange", 0.25), ("banana", 0.20))

  def checkout(items: Array[String]): BigDecimal = {
    val total = items.flatMap(price).sum
    total.setScale(2, RoundingMode.HALF_EVEN)
  }

  def checkoutWithOffers(items: Array[String]): BigDecimal = {
    val noOfApples: Int = items.count(p => p.equalsIgnoreCase("apple"))
    val noOfOranges: Int = items.count(p => p.equalsIgnoreCase("orange"))
    val noOfBananas: Int = items.count(p => p.equalsIgnoreCase("banana"))

    val total = Offers.offer("apple")(noOfApples) + Offers.offer("orange")(noOfOranges) + Offers.offer("banana")(noOfBananas)
    total.setScale(2, RoundingMode.HALF_EVEN)
  }

  def getPrice(item: String): BigDecimal = {
    productPriceList.getOrElse(item toLowerCase(), 0)
  }

  def price(item: String): Option[BigDecimal] = productPriceList.get(item toLowerCase)

}
