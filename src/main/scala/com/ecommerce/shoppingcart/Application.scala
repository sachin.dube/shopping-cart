package com.ecommerce.shoppingcart

object Application {

  def main(args: Array[String]) {
   println("Offer Price : [ " + args.mkString(", ") + " ] => £" + ShoppingCart.checkoutWithOffers(args))
  }

}
