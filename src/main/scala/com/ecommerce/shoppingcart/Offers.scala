package com.ecommerce.shoppingcart

object Offers {

  def buyOneGetOne(numberOfItems: Int, costOfOne: BigDecimal): BigDecimal = ((numberOfItems / 2) + (numberOfItems % 2)) * costOfOne

  def threeForTwo(numberOfItems: Int, costOfOne: BigDecimal): BigDecimal = (2 * (numberOfItems/3) + (numberOfItems %3)) * costOfOne

  def offer(item: String): Int => BigDecimal = {
    val price: BigDecimal = ShoppingCart.getPrice(item)
    item toLowerCase() match {
      case "apple" => (numberOfItems:Int) => buyOneGetOne(numberOfItems, price).setScale(2, BigDecimal.RoundingMode.HALF_EVEN)
      case "banana" => (numberOfItems:Int) => buyOneGetOne(numberOfItems, price).setScale(2, BigDecimal.RoundingMode.HALF_EVEN)
      case "orange" => (numberOfItems:Int) => threeForTwo(numberOfItems, price).setScale(2, BigDecimal.RoundingMode.HALF_EVEN)
      case _ => (numberOfItems:Int) => numberOfItems * price
    }
  }

}
